from library_inventory.app import application, db

from config import DEBUG, HOST, PORT


db.create_all()
application.run(debug=DEBUG, host=HOST, port=PORT)
