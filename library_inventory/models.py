from datetime import datetime, timedelta

from library_inventory.app import db


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True, nullable=False)
    name = db.Column(db.String(80), nullable=False)
    email = db.Column(db.String(80), unique=True, nullable=False)
    birth_date = db.Column(db.DateTime, nullable=False)
    penalized = db.Column(db.Boolean, default=False)
    holds = db.relationship('Hold', backref='user', lazy='dynamic')

    def __init__(self, name, email, birth_date):
        self.name = name
        self.email = email
        self.birth_date = datetime.strptime(birth_date, '%b %d %Y')

    def __repr__(self):
        return '<User {}>'.format(self.name)

    def to_dict(self):
        return {
            'name': self.name,
            'email': self.email,
            'birth_date': self.birth_date,
            'penalized': self.penalized
        }

    def add(self):
        db.session.add(self)
        db.session.commit()

    def modify(self, name, email, birth_date):
        if name is not None:
            self.name = name
        if birth_date is not None:
            self.birth_date = datetime.strptime(birth_date, '%b %d %Y')
        db.session.commit()

    def delete(self):
        db.session.delete(self)
        db.session.commit()


class Book(db.Model):
    id = db.Column(db.Integer, primary_key=True, nullable=False)
    isbn = db.Column(db.String(17), unique=True, nullable=False)
    name = db.Column(db.String(80), nullable=False)
    author = db.Column(db.String(80), nullable=False)
    book_format = db.Column(db.String(40), nullable=False)
    language = db.Column(db.String(30), nullable=False)
    description = db.Column(db.Text)
    pub_date = db.Column(db.DateTime)
    stock = db.Column(db.Integer, default=0)
    holds = db.relationship('Hold', backref='book', lazy='dynamic')

    def __init__(self, isbn, name, author, book_format, language, description,
                 pub_date, stock=0):
        self.isbn = isbn
        self.name = name
        self.author = author
        self.book_format = book_format
        self.language = language
        self.description = description
        self.pub_date = datetime.strptime(pub_date, '%b %d %Y')
        self.stock = stock

    def __repr__(self):
        return '<Book {}>'.format(self.isbn)

    def to_dict(self):
        return {
            'isbn': self.isbn,
            'name': self.name,
            'author': self.author,
            'book_format': self.book_format,
            'language': self.language,
            'description': self.description,
            'pub_date': self.pub_date,
            'stock': self.stock
        }

    @property
    def available(self):
        return self.stock > 0

    def add(self):
        db.session.add(self)
        db.session.commit()

    def modify(self, isbn, name, author, book_format, language, description,
               pub_date, stock):
        if isbn is not None:
            self.isbn = isbn
        if name is not None:
            self.name = name
        if author is not None:
            self.author = author
        if book_format is not None:
            self.book_format = book_format
        if language is not None:
            self.language = language
        if description is not None:
            self.description = description
        if pub_date is not None:
            self.pub_date = datetime.strptime(pub_date, '%b %d %Y')
        if stock is not None:
            self.stock = stock

        db.session.commit()

    def delete(self):
        db.session.delete(self)
        db.session.commit()

    def remove_stock(self, quantity=1):
        self.stock -= quantity
        db.session.commit()

    def add_stock(self, quantity=1):
        self.stock += quantity
        db.session.commit()


class Hold(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    book_id = db.Column(db.Integer, db.ForeignKey('book.id'), nullable=False)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    hold_date = db.Column(db.DateTime, nullable=False)
    return_date = db.Column(db.DateTime, nullable=False)
    observations = db.Column(db.Text, nullable=True)
    returned = db.Column(db.Boolean, default=False)

    def __init__(self, book_id, user_id, ret_days=7):
        self.book_id = book_id
        self.user_id = user_id
        self.hold_date = datetime.utcnow()
        self.return_date = self.hold_date + timedelta(days=ret_days)

    def __repr__(self):
        return '<Hold {}>'.format(self.id)

    def to_dict(self):
        return {
            'hold_id': self.id,
            'book_id': self.book_id,
            'user_id': self.user_id,
            'hold_date': self.hold_date,
            'return_date': self.return_date,
            'observations': self.observations,
            'returned': self.returned
        }

    def place(self):
        db.session.add(self)
        db.session.commit()
        self.book.remove_stock()

    def register_return(self, ret_date=None, observations=None):
        if observations is None:
            observations = ''

        self.observations = observations
        self.returned = True

        if ret_date is None:
            ret_date = datetime.utcnow()
        else:
            ret_date = datetime.strptime(ret_date, '%b %d %Y')

        if ret_date > self.return_date:
            self.user.penalized = True

        db.session.commit()
        self.book.add_stock()
