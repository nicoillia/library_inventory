import unittest

from datetime import datetime, timedelta
from json import dumps, loads

from library_inventory.app import application, db


USER_1_DATA = {
    'name': 'Juan',
    'email': 'juan@example.com',
    'birth_date': 'Jan 10 1991'
}
USER_2_DATA = {
    'name': 'Maria',
    'email': 'maria@example.com',
    'birth_date': 'Jul 21 1987'
}

BOOK_1_DATA = {
    'isbn': '9780553293357',
    'name': 'Foundation',
    'author': 'Isaac Asimov',
    'book_format': 'Paperback',
    'language': 'English',
    'description': 'For twelve thousand years the Galactic Empire has...',
    'pub_date': 'Dec 01 2004',
    'stock': 3
}
BOOK_2_DATA = {
    'isbn': '9781451690316',
    'name': 'Fahrenheit 451',
    'author': 'Ray Bradbury',
    'book_format': 'Paperback',
    'language': 'Spanish',
    'description': 'Guy Montag is a fireman. In his world, where tele...',
    'pub_date': 'May 23 2012',
    'stock': 1
}


class TestBase(unittest.TestCase):

    def setUp(self):
        self.app = application.test_client()
        self.app.testing = True
        application.config['SQLALCHEMY_DATABASE_URI'] = "sqlite://"
        db.create_all()

    def tearDown(self):
        pass

    def test_01_create_user(self):
        result = self.app.post('/users/', data=dumps(USER_1_DATA),
                               content_type='application/json')

        # assert the status code of the response
        self.assertEqual(result.status_code, 200)

        # Getting json response
        json_response = loads(result.data.decode('utf8'))
        # Assert user_id in response
        self.assertIn('user_id', json_response)

    def test_02_get_created_user(self):
        result = self.app.get('/users/1/')

        # assert the status code of the response
        self.assertEqual(result.status_code, 200)

        # Getting json response
        json_response = loads(result.data.decode('utf8'))
        # Assert email in response
        self.assertIn('email', json_response)

        # Assert the email of the user created
        self.assertEqual(USER_1_DATA['email'], json_response['email'])

    def test_03_create_duplicated_user(self):
        result = self.app.post('/users/', data=dumps(USER_1_DATA),
                               content_type='application/json')

        # assert the status code of the response
        self.assertEqual(result.status_code, 200)

        # Getting json response
        json_response = loads(result.data.decode('utf8'))
        # Assert error in response
        self.assertIn('error', json_response)

    def test_04_get_users(self):
        result = self.app.get('/users/')

        # assert the status code of the response
        self.assertEqual(result.status_code, 200)

    def test_05_modify_user(self):
        USER_1_DATA['name'] += ' Carlos'

        result = self.app.put('/users/1/', data=dumps(USER_1_DATA),
                              content_type='application/json')

        # assert the status code of the response
        self.assertEqual(result.status_code, 200)

        result = self.app.get('/users/1/')
        # Getting json response
        json_response = loads(result.data.decode('utf8'))
        # Assert name in response
        self.assertIn('name', json_response)

        # Assert the name of the user created
        self.assertEqual(USER_1_DATA['name'], json_response['name'])

    def test_06_delete_user(self):
        self.app.post('/users/', data=dumps(USER_2_DATA),
                      content_type='application/json')

        result = self.app.delete('/users/2/')

        # assert the status code of the response
        self.assertEqual(result.status_code, 200)

        result = self.app.get('/users/2/')
        # assert the status code of the response
        self.assertEqual(result.status_code, 404)

    def test_07_create_book(self):
        result = self.app.post('/books/', data=dumps(BOOK_1_DATA),
                               content_type='application/json')

        # assert the status code of the response
        self.assertEqual(result.status_code, 200)

        # Getting json response
        json_response = loads(result.data.decode('utf8'))
        # Assert book_id in response
        self.assertIn('book_id', json_response)

    def test_08_get_existing_book(self):
        result = self.app.get('/books/1/')

        # assert the status code of the response
        self.assertEqual(result.status_code, 200)

        # Getting json response
        json_response = loads(result.data.decode('utf8'))
        # Assert isbn in response
        self.assertIn('isbn', json_response)

        # Assert the isbn of the user created
        self.assertEqual(BOOK_1_DATA['isbn'], json_response['isbn'])

    def test_09_create_duplicated_book(self):
        result = self.app.post('/books/', data=dumps(BOOK_1_DATA),
                               content_type='application/json')

        # assert the status code of the response
        self.assertEqual(result.status_code, 200)

        # Getting json response
        json_response = loads(result.data.decode('utf8'))
        # Assert error in response
        self.assertIn('error', json_response)

    def test_10_get_books(self):
        result = self.app.get('/books/')

        # assert the status code of the response
        self.assertEqual(result.status_code, 200)

    def test_11_modify_book(self):
        BOOK_1_DATA['language'] = 'Spanish'

        result = self.app.put('/books/1/', data=dumps(BOOK_1_DATA),
                              content_type='application/json')

        # assert the status code of the response
        self.assertEqual(result.status_code, 200)

        result = self.app.get('/books/1/')
        # Getting json response
        json_response = loads(result.data.decode('utf8'))
        # Assert language in response
        self.assertIn('language', json_response)

        # Assert the language of the user created
        self.assertEqual(BOOK_1_DATA['language'], json_response['language'])

    def test_12_delete_book(self):
        self.app.post('/books/', data=dumps(BOOK_2_DATA),
                      content_type='application/json')

        result = self.app.delete('/books/2/')

        # assert the status code of the response
        self.assertEqual(result.status_code, 200)

        result = self.app.get('/users/2/')
        # assert the status code of the response
        self.assertEqual(result.status_code, 404)

    def test_13_place_hold(self):
        result = self.app.post('/holds/', data=dumps({'book_id': '1',
                                                      'user_id': '1'}),
                               content_type='application/json')

        # assert the status code of the response
        self.assertEqual(result.status_code, 200)

    def test_14_register_return(self):
        result = self.app.put('/holds/1/', data=dumps({}),
                              content_type='application/json')

        # assert the status code of the response
        self.assertEqual(result.status_code, 200)

    def test_15_get_holds(self):
        result = self.app.get('/holds/')

        # assert the status code of the response
        self.assertEqual(result.status_code, 200)

    def test_16_expired_hold(self):
        self.app.post('/holds/', data=dumps({'book_id': '1', 'user_id': '1'}),
                      content_type='application/json')

        ret_date = datetime.utcnow() + timedelta(days=14)
        ret_date = ret_date.strftime('%b %d %Y')
        self.app.put('/holds/2/', data=dumps({'ret_date': ret_date}),
                     content_type='application/json')

        result = self.app.get('/users/1/')
        # Getting json response
        json_response = loads(result.data.decode('utf8'))
        # Assert penalized in response
        self.assertIn('penalized', json_response)
        # Assert the penalized field of the user created
        self.assertEqual(True, json_response['penalized'])
