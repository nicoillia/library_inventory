from flask import jsonify, request
from flask.views import MethodView
from sqlalchemy import exc

from library_inventory.app import application
from library_inventory.models import Book, Hold, User


class UserApi(MethodView):
    """ Users API """

    def get(self, user_id):
        response = {}
        if user_id is not None:
            user = User.query.filter_by(id=user_id).first_or_404()
            if user is not None:
                response = user.to_dict()
        else:
            response = []
            users = User.query.all()
            for user in users:
                response.append({'user_id': user.id, 'email': user.email})

        return jsonify(response)

    def post(self):
        response = {}
        data = request.get_json()
        try:
            user = User(**data)
            user.add()
        except TypeError as e:
            response = {'error': 'missing data'}
        except exc.IntegrityError as e:
            response = {'error': 'existing user'}
        else:
            response = {'user_id': user.id}

        return jsonify(response)

    def put(self, user_id):
        response = {}
        user = User.query.filter_by(id=user_id).first_or_404()
        if user is not None:
            data = request.get_json()
            user.modify(**data)

        return jsonify(response)

    def delete(self, user_id):
        response = {}
        user = User.query.filter_by(id=user_id).first_or_404()
        if user is not None:
            user.delete()

        return jsonify(response)


class BookApi(MethodView):
    """ Books API """

    def get(self, book_id):
        response = {}
        if book_id is not None:
            book = Book.query.filter_by(id=book_id).first_or_404()
            if book is not None:
                response = book.to_dict()
        else:
            response = []
            books = Book.query.all()
            for book in books:
                response.append({'book_id': book.id,
                                 'isbn': book.isbn,
                                 'name': book.name,
                                 'available': book.available})

        return jsonify(response)

    def post(self):
        response = {}
        data = request.get_json()
        try:
            book = Book(**data)
            book.add()
        except TypeError as e:
            response = {'error': 'missing data'}
        except exc.IntegrityError as e:
            response = {'error': 'book duplicated'}
        else:
            response = {'book_id': book.id}

        return jsonify(response)

    def put(self, book_id):
        response = {}
        book = Book.query.filter_by(id=book_id).first_or_404()
        if book is not None:
            data = request.get_json()
            book.modify(**data)

        return jsonify(response)

    def delete(self, book_id):
        response = {}
        book = Book.query.filter_by(id=book_id).first_or_404()
        if book is not None:
            book.delete()

        return jsonify(response)


class HoldApi(MethodView):
    """ Holds API """

    def get(self, hold_id):
        if hold_id is not None:
            hold = Book.query.filter_by(id=hold_id).first_or_404()
            if hold is not None:
                response = hold.to_dict()
        else:
            response = []
            holds = Hold.query.all()
            for hold in holds:
                response.append({'hold_id': hold.id,
                                 'book_id': hold.book_id,
                                 'user_id': hold.user_id,
                                 'hold_date': hold.hold_date,
                                 'returned': hold.returned})

        return jsonify(response)

    def post(self):
        response = {}
        data = request.get_json()

        _user_id = data.get('user_id', None)
        _book_id = data.get('book_id', None)
        user = User.query.filter_by(id=_user_id).first()
        book = Book.query.filter_by(id=_book_id).first()

        if user is None:
            response = {'error': 'user not exists'}
        elif book is None:
            response = {'error': 'book not exists'}
        elif book is not None and User is not None:
            if not user.penalized:
                if book.available:
                    hold = Hold(**data)
                    hold.place()
                    response = hold.to_dict()
                else:
                    response = {'error': 'book unavailable'}
            else:
                response = {'error': 'user penalized'}

        return jsonify(response)

    def put(self, hold_id):
        response = {}
        hold = Hold.query.filter_by(id=hold_id).first_or_404()
        if hold is not None:
            data = request.get_json()
            hold.register_return(**data)

        return jsonify(response)


user_api_view = UserApi.as_view('user_api')
application.add_url_rule('/users/', methods=['GET'],
                         view_func=user_api_view, defaults={'user_id': None})
application.add_url_rule('/users/', methods=['POST'],
                         view_func=user_api_view)
application.add_url_rule('/users/<int:user_id>/',
                         methods=['GET', 'PUT', 'DELETE'],
                         view_func=user_api_view)

book_api_view = BookApi.as_view('book_api')
application.add_url_rule('/books/', methods=['GET'],
                         view_func=book_api_view, defaults={'book_id': None})
application.add_url_rule('/books/', methods=['POST'],
                         view_func=book_api_view)
application.add_url_rule('/books/<int:book_id>/',
                         methods=['GET', 'PUT', 'DELETE'],
                         view_func=book_api_view)

hold_api_view = HoldApi.as_view('hold_api')
application.add_url_rule('/holds/', methods=['GET'],
                         view_func=hold_api_view, defaults={'hold_id': None})
application.add_url_rule('/holds/', methods=['POST'],
                         view_func=hold_api_view)
application.add_url_rule('/holds/<int:hold_id>/', methods=['PUT'],
                         view_func=hold_api_view)
