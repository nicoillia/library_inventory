from flask import Flask
from flask_sqlalchemy import SQLAlchemy

from config import DATABASE_URI


application = Flask(__name__)
application.config['SQLALCHEMY_DATABASE_URI'] = DATABASE_URI

db = SQLAlchemy(application)

from library_inventory import views
