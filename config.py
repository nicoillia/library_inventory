from os.path import abspath, dirname, join


DEBUG = True

HOST = "127.0.0.1"
PORT = 9000

DATABASE_URI = 'sqlite:///' + join(abspath(dirname(__file__)), 'app.db')
