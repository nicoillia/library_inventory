#!/bin/bash

HOST="http://localhost:9000"
USERS_API_ENDPOINT="users"
BOOKS_API_ENDPOINT="books"
HOLDS_API_ENDPOINT="holds"

###
# Creating users
###
curl -H 'Content-Type: application/json' \
     -X POST \
     -d '{"name": "Juan",
          "email": "juan@example.com",
          "birth_date": "Jan 10 1991"}' $HOST/$USERS_API_ENDPOINT/

curl -H 'Content-Type: application/json' \
     -X POST \
     -d '{"name": "Maria",
         "email": "maria@example.com",
         "birth_date": "Jul 21 1987"}' $HOST/$USERS_API_ENDPOINT/

###
# Creating books
###
curl -H 'Content-Type: application/json' \
     -X POST \
     -d '{"isbn": "9780553293357",
          "name": "Foundation",
          "author": "Isaac Asimov",
          "book_format": "Paperback",
          "language": "English",
          "description": "For twelve thousand years the Galactic Empire has...",
          "pub_date": "Dec 01 2004",
          "stock": 3}' $HOST/$BOOKS_API_ENDPOINT/

curl -H 'Content-Type: application/json' \
     -X POST \
     -d '{"isbn": "9781451690316",
          "name": "Fahrenheit 451",
          "author": "Ray Bradbury",
          "book_format": "Paperback",
          "language": "Spanish",
          "description": "Guy Montag is a fireman. In his world, where tele...",
          "pub_date": "May 23 2012",
          "stock": 1}' $HOST/$BOOKS_API_ENDPOINT/

###
# Creating holds
###

curl -H 'Content-Type: application/json' \
     -X POST \
     -d '{"book_id": "1",
          "user_id": "1"}' $HOST/$HOLDS_API_ENDPOINT/

curl -H 'Content-Type: application/json' \
     -X POST \
     -d '{"book_id": "2",
          "user_id": "1"}' $HOST/$HOLDS_API_ENDPOINT/

curl -H 'Content-Type: application/json' \
     -X POST \
     -d '{"book_id": "1",
          "user_id": "2"}' $HOST/$HOLDS_API_ENDPOINT/
